=====================
DEPENDENCES:
python2.7
swig2.0
numpy
matplotlib
pyphystat module (see note below about environment variables)

======================
COMPILATION

$make

======================
RUN
First export the path to the directory where pyschell and
pyphystat directories are located.

$export PYTHONPATH="/home/myusername/python_modules"

Only then will you be able to run the python scripts:

$python testSchelling.py
(NB: you must close plot windows as they appear to continue).
