/*
ORIGINAL CODE BY Laetitia Gauvin 
http://lps.ens.fr/~laetitia/
PhD thesis: https://hal.archives-ouvertes.fr/tel-00556769

NB: 
struct couple {int a;int b;};
std::vector< int > liste_value;           case pleine: valeur 
     agent normal: -1,1
     agent switch: 2
std::vector< couple> liste_position;      case pleine: position 
lv_ptr : pointeur vers liste_value
lp_ptr : pointeur vers liste_position
array:  case noire: array[index]=1; case blanche: array[index]=-1;
        condition aux limites: 2        
Swig c++/Python interface by A.Hazan.
*/

/* 
   TODO: 
   - input = array with A,B non-switching agents, and A/B switching agents
   - same encoding for input and output array (right now, that's not the case)
*/

#include <stdlib.h>
#include <stdio.h>
#include<math.h>
#include<iostream>
#include<fstream>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <vector>
#include <algorithm>
#include <numeric>
#include "boost/multi_array.hpp"

#include "Schelling.h"
#define DEBUG 1

using namespace std;


int gt=5;  
typedef boost::rand48 base_generator_type;
base_generator_type generator(gt);
boost::uniform_real<> uni_dist(0,1);
boost::variate_generator<base_generator_type&,boost::uniform_real<> >uni(generator,uni_dist);


typedef boost::multi_array<int, 1> one_i_array_type;
typedef boost::multi_array<double, 2> two_d_array_type;
typedef boost::multi_array<int, 2> two_i_array_type;

struct couple {int a;int b;};
struct sgl {double e;};

/*
  Initialization of switching agents on the grid: standard case
  (switching agents chosen at random on the grid)  
 */
void init_grid_switch(int* array, int rows, int cols, double Occ, double agt_switch, std::vector< int > *lv_ptr,std::vector< couple> *lp_ptr){
  int N,i,m,n;
  double x,y;

  int index;
  couple c;
  N=rows-2;
  if (DEBUG){cout<<"remplissage switch, imax= "<< N*N*agt_switch+1 <<endl;}
  
  for(i=1;i<N*N*agt_switch*Occ+1;i++)  // FIX AH:  i<N*N*agt_switch+1       
    {
      do{x=(rand()+1.)/((double)RAND_MAX+1.);				 
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.);
	n=ceil(N*y);
	index = n*rows + m; // FIX AH
      }            
      while(array[index]!=0);
      
      if(uni()<0.5) 
	{array[index]=-1;}
      else
	{array[index]=1;}
      c.a=m;
      c.b=n;
      //liste_position.push_back(c);
      //liste_value.push_back(2);
      lp_ptr->push_back(c);
      lv_ptr->push_back(2);
    }  
}

/*
  Initialization of switching agents on the grid using values in array:
  +1 and -1 in input array are considered as switching agents.  
 */
void init_grid_switch_from_array(int* array, int rows, int cols, double Occ, double agt_switch, std::vector< int > *lv_ptr,std::vector< couple> *lp_ptr){
  int N,i,j;
  int index;
  couple c;
  N=rows-2;
  if (DEBUG){cout<<"init_grid_switch_from_array"<< "rows="<<rows<<" cols="<< cols <<" agt_switch="<< agt_switch <<" lv_ptr="<<lv_ptr<<" lp_ptr="<< lp_ptr <<endl;}
  
  for(i=1;i<N+1;i++)               
    for(j=1;j<N+1;j++)     
      {{
	  index = j*rows + i; // FIX AH
	  if((array[index]==1)||(array[index]==-1) ){ 
	    c.a=i; c.b=j; 
	    lp_ptr->push_back(c); lv_ptr->push_back(2);	  
	  }
	}}
  
}

/*
  Simulation   
 */
int Sim(int* array, int rows, int cols, double X, double Occ,double agt_switch, double p_u, double p_f, int init, int switch_active, int nb_iter)
//int main(int argc, char *argv[])
{
  int N=rows-2;
  int nomove;
  double x,y;
  double p_st,p_test;
  int i,j,p,k;
  int u;
  int m,n,mf,nf,agt,state_agt;
  int Ni,Nv,Nd;
  int ct;
  double p_h=0 ; //????????????????????????????,

  int index;
  int it=100; //FIX AH: 10000-> 1000
  //int P=15000000;//50*N*N+1000*it;
  std::vector<int> lb;
  std::vector<int> cluster;
  std::vector<int> cluster2;
  std::vector<int> cluster3;
  std::vector<double> fcluster;
  std::vector<double> fcluster2;
  std::vector<double> fcluster3;
  std::vector<double> meansizecluster;
  std::vector<double> meansizecluster2;
  std::vector<double> meansizecluster3;
  std::vector< int > liste_value;          /* case pleine: valeur */
  std::vector< couple> liste_position;     /* case pleine: position */
  std::vector< int > *lv_ptr;
  std::vector< couple> *lp_ptr;

  std::vector< couple> liste_sp;            /* case vide: position */
  std::vector<couple> neighbour;
  couple c;
  
  if (DEBUG){cout << "rows= "<<rows<<" cols="<< cols<< " X="<< X <<" Occ=" << Occ<< " agt_switch=" << agt_switch << " p_u="<< p_u <<" p_f="<< p_f <<" nb_iter="<<nb_iter<<endl;}
  N=rows-2;
  
  /*CHECK ARGS:*/
  if(Occ<0 || Occ>1 || agt_switch<0 || agt_switch>1|| p_u<0||p_u >1|| p_f<0||p_f >1|| nb_iter<0)
    return -1;

  /*AGENTS SWITCH: remplissage par cases noires et blanches*/   
  lv_ptr=&liste_value;
  lp_ptr=&liste_position;
  switch(init){
  case 0:
    init_grid_switch( array, rows, cols, Occ, agt_switch, lv_ptr, lp_ptr);
  case 1:
    init_grid_switch_from_array( array, rows, cols, Occ, agt_switch, lv_ptr, lp_ptr);
  }
  
  /*AGENTS NON-SWITCH: remplissage par cases noires et blanches*/          
  for(i=1;i<N*N*Occ/2+1;i++)
    /*remplissage du réseau par des cases noires et blanches*/              
    {
      do{x=(rand()+1.)/((double)RAND_MAX+1.);
	/*remplissage par des cases noires*/
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.);
	n=ceil(N*y);
	index = n*rows + m; // FIX AH
      }
      while(array[index]!=0);
      
      array[index]=1;
      c.a=m;
      c.b=n;
      liste_position.push_back(c);
      liste_value.push_back(1);
      
      do	{x=(rand()+1.)/((double)RAND_MAX+1.);			
	/*remplissage par des cases blanches*/
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.);
	n=ceil(N*y);
	index = n*rows + m; // FIX AH
      }
      while(array[index]!=0);
      
      array[index]=-1;
      c.a=m;
      c.b=n;
      liste_position.push_back(c);
      liste_value.push_back(-1);
    }
   
  /* case vides: enregistrement de leurs positions dans liste_sp */
  for(i=1;i<N+1;i++)               
    for(j=1;j<N+1;j++)     
      {{
	  index = j*rows + i; // FIX AH
	  if(array[index]==0) {c.a=i; c.b=j; liste_sp.push_back(c);}
	  
	}}
  if (DEBUG){cout<<"size="<<liste_position.size()<<endl;}
  
  boost::uniform_int<> unilist_dist(1,liste_position.size());
  boost::variate_generator<base_generator_type&,boost::uniform_int<> >unilist(generator,unilist_dist);
  boost::uniform_int<> unilistsp_dist(1,liste_sp.size());
  boost::variate_generator<base_generator_type&,boost::uniform_int<> >unilistsp(generator,unilistsp_dist);
  
  /*conditions aux limites */
  if (DEBUG){cout<<"conditions aux limites"<<endl;}
  for(j=0;j<N+2;j++)
    {
      index = j*rows + 0; // FIX AH
      array[index]=2;
      index = j*rows + N+1;
      array[index]=2;
      index = (N+1)*rows + j;
      array[index]=2;
      index = 0*rows + j; // FIX AH
      array[index]=2;
    }

  if (DEBUG){cout<<"MAIN LOOP "<<endl;}
  for(p=0;p<nb_iter;p++) /*départ de la boucle d'iteration*/
    { //printf("%d\n ",p);
      
      if(nomove>N*N) {
	//if (DEBUG){cout<<"break"<<endl;} 
	//break;
      }
      
      agt=unilist();

      m=liste_position[agt-1].a;
      n=liste_position[agt-1].b;      
      
      /*calcul du nombre de voisins*/
      Ni=0;
      Nv=0;
      state_agt=liste_value[agt-1];
      //cpt=-1;

      if(state_agt!=2)/* AGENTS NON SWITCH */
	{for(k=0;k<2;k++)
	    { index = (n+k)*rows + m-1; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n+1)*rows + m+k; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n-1)*rows + m-k; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n-k)*rows + m+1; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n+k)*rows + m-1; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}
	      index = (n+1)*rows + m+k; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}
	      index = (n-1)*rows + m-k; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}
	      index = (n-k)*rows + m+1; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}	      
	    }	  
	  
	  if(m==1|| n==1||m==N||n==N)
	    Nd=5-Ni-Nv;
	  if(m==1&&n==1 ||m==1&&n==N || m==N&&n==1 ||m==N&&n==N)
	    Nd=3-Ni-Nv;     
	  if(m!=1 && n!=1 && m!=N && n!=N)
	    Nd=8-Ni-Nv;
	  
 
	  if(Nd>X*(Nd+Ni)) p_st=p_u;
	  else p_st=p_h;
	  
	  u=array[n*rows + m];	 	  
	  
	  /////////////////////
	  ct=0;
	  random_shuffle ( liste_sp.begin(), liste_sp.end() );

	  /* choix d'une nouvelle position pour l'individu*/
	  do{
	    mf=liste_sp[ct].a;
	    nf=liste_sp[ct].b;
	    
	    ct++;
	    /*calcul du nombre de voisins dans cette nouvelle position*/
	    Ni=0;
	    Nv=0;	    
	    array[n*rows + m]=0;   
	    
	    for(k=0;k<2;k++)
	      {
		index = (nf+k)*rows + mf-1;
		if(array[index]==0) {Nv++;}
		index = (nf+1)*rows + mf+k;
		if(array[index]==0) {Nv++;}
		index = (nf-1)*rows + mf-k;
		if(array[index]==0) {Nv++;}
		index = (nf-k)*rows + mf+1;
		if(array[index]==0) {Nv++;}
		index = (nf+k)*rows + mf-1;
		if(array[index]==u) {Ni++;}
		index = (nf+1)*rows + mf+k;
		if(array[index]==u) {Ni++;}
		index = (nf-1)*rows + mf-k;
		if(array[index]==u) {Ni++;}
		index = (nf-k)*rows + mf+1;
		if(array[index]==u) {Ni++;}		
	      }
	    if(mf==1|| nf==1||mf==N||nf==N)
	      Nd=5-Ni-Nv;
	    if(mf==1&&n==1 ||mf==1&&nf==N || mf==N&&nf==1 ||mf==N&&nf==N)
	      Nd=3-Ni-Nv;     
	    if(mf!=1&&nf!=1&&mf!=N&&nf!=N)
	      Nd=8-Ni-Nv;
	    
	  }
	  while(Nd>X*(Nd+Ni) && ct<liste_sp.size());
	  p_test=uni();
	  
	  if(Nd<=X*(Nd+Ni) && p_test<=p_st ) /* si l'agent est satisfait dans cette nouvelle position*/	    
	    {
	      array[nf*rows + mf]=u ;
	      liste_position[agt-1].a=mf;
	      liste_position[agt-1].b=nf;
	      liste_sp[ct-1].a=m;liste_sp[ct-1].b=n;	      
	      nomove=0;
	    }else{
	    array[n*rows + m]=u;// S[m][n]=u; 
	    nomove++; 
	    continue;}	  
	}
      else{  /* AGENTS SWITCH: flip */
	if((switch_active==1) && ( uni() <=p_f)) {  // FIX AH p_f<= 0.05
	  array[n*rows + m]= -array[n*rows + m]; // S[m][n]=-S[m][n]
	    };
      }
      
    }	
  return p;
}

