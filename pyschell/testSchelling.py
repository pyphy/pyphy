# -*- coding: utf-8

"""
Python wrapper to schelling simulator in c++
(see testMatrix.py)

coded by Aurélien Hazan
"""

# Import NumPy
import numpy as np
import Schelling
import matplotlib.pylab as plt
from pyphystat.rng import circle_rng
import pyphystat.Pyphystat
from pyphystat.information_index import block_count, information_index

typeCode = "i"  # type of the matrix. i=integer
N = 100          # grid size
nb_iter = 100000  # number of time steps
X= 0.3          # tolerance
Occ=0.7       # occupation density
agt_switch=0.0  # switching agents proportion
p_u=0.1         # probability to jump if unhappy
p_f=0.01        # probability to switch 
init = 0       # matrix initialization (0: random; 1: array)
switch_active= 1 # activate switch agents
iface_dens =  pyphystat.Pyphystat.__dict__["interface_density"]

# random init
mat = np.zeros((N,N),typeCode)
mat0 = np.copy(mat)
sim = Schelling.__dict__["Sim"]
print X, Occ,agt_switch, p_u, p_f, init, switch_active, nb_iter
p=sim(mat, X, Occ,agt_switch, p_u, p_f, init, switch_active, nb_iter)
# compute order parameters
x0=iface_dens(mat0)
x=iface_dens(mat)
print "============\nUniform init:\n"
print "number of time steps=",p
print "interface density x0=",x0
print "interface density x=",x
count = block_count(mat,N/10)
H = information_index(count)
print "information index=",H, " blocksize=",N/10
# plot
plt.matshow(mat)
plt.title("uniform initialisation")
print "\nCLOSE WINDOW TO CONTINUE...\n"
plt.show()


# random circle init
n_pts=1000 #400
rad=30
sig=1 #
rnd_seed=1
mat = circle_rng(N, n_pts, rad, sig, rnd_seed)
mat0 = np.copy(mat)
init = 1       # initialization (0: random; 1: array)
p=sim(mat, X, Occ,agt_switch, p_u, p_f, init, switch_active, nb_iter)

# compute order parameters
x0=iface_dens(mat0)
x=iface_dens(mat)
print "============\nUniform init:\n"
print "number of time steps=",p
print "interface density x0=",x0
print "interface density x=",x

count = block_count(mat,N/10)
H = information_index(count)
print "information index=",H, " blocksize=",N/10

plt.matshow(mat0)
plt.matshow(mat)
plt.title("random circle initialisation")
print "\nCLOSE WINDOW TO CONTINUE...\n"
plt.show()

