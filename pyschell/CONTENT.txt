CONTENT:

* python wrapper to c++ functions

Makefile         : makefile.
Schelling.cxx    : set of c++ functions.
Schelling.h      : header file
testSchelling.py : test script. 
Schelling.i      : swig interface to Schelling.cxx
setup.py         : module-building python script.
numpy.i          : swig interface file to numpy.

* other:
README.txt     : how to compile, run.
TODO.txt       : list of future improvements.
