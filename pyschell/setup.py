#! /usr/bin/env python

# System imports
from distutils.core import *
from distutils      import sysconfig

# Third-party modules - we depend on numpy for everything
import numpy

# Obtain the numpy include directory.
numpy_include = numpy.get_include()


# _Matrix extension module
_Schelling = Extension("_Schelling",
                    ["Schelling_wrap.cxx",
                     "Schelling.cxx"],
                    include_dirs = [numpy_include],
                    )

# NumyTypemapTests setup
setup(name        = "NumpyTypemapTests",
      description = "Functions that work on arrays",
      author      = "Bill Spotz",
      #py_modules  = ["Array", "Farray", "Vector", "Matrix", "Tensor",
      #               "Fortran"],
      py_modules  = [],
      ext_modules = [ _Schelling ]
      )
