======================
LICENSE:
This code is distributed under the GPLv3. 
You will find a copy in this directory.
See also:
https://gnu.org/licenses/gpl.html
http://en.wikipedia.org/wiki/GNU_General_Public_License

======================
DEPENDENCES:
python2
numpy               (debian package: python-numpy)
matplotlib          (debian package: python-matplotlib)   
pytables            (debian package: python-tables)
pyschell, pyschell  (https://gitorious.org/pyphy)

Optional:
vitables            (debian package: vitables)
ipython             (debian package: ipython, ipython-notebook)

======================
COMPILATION:

none

======================
RUN:

Computation and plotting are written in separate files.

First create a directory where the data will be written.

Then run a data-generating script, for example:
$python reardon_schelling_compute.py
NB: you might want to tune the parameters in the "variables" section
    of the script to start with less parameters values, and increase speed.

The data will be written in a hdf5 file by pytables.
(modify the "filename" parameter in the python script)

You may want to inspect the hdf5 file directly with vitables
(see "Dependences" above).

Then run a plotting script, for example:
$python reardon_schelling_plot.py

======================
WRITE YOUR OWN EXPERIMENT:

see template_compute.py and template_plot.py 

======================
CREATE AN IPYTHON NOTEBOOK:

Ipython notebooks are interactive ipython shells 
embedded in a browser. They are  useful for:
* live demos and teaching.
* quick exports to pdf or html (without
the need to write a tex file)
* automatically sharing the notebook on the internet 
using http://nbviewer.ipython.org/ 

Example:
$ipython notebook reardon_schelling_notebook.ipynb


======================
TODOLIST:

* parallelize (for multicore first).
* code some Design of Experiment (DoE) parameter choice, such as latin
  hypercube, or NOLH.
* code some statistical analysis of the influence of parameters.
 
