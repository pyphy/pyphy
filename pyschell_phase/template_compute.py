# -*- coding: utf-8

"""
Compute the phase diagram of ...

Writes the results in a hdf5 file, with pytables.

To plot the results: see thisfilename_plot.py

Reference:
http://fr.arxiv.org/abs/???

Python doc:
pytables:   http://pytables.github.io/usersguide/tutorials.html
            (see sections: #dealing-with-nested-structures-in-tables)
            see also soft/external links
itertools:  https://docs.python.org/2/library/itertools.html
matplotlib: http://matplotlib.sourceforge.net

coded by YOURNAME, DATE.
License: see README.txt
"""
# import general purpose modules
import numpy as np
import matplotlib.pylab as plt
from tables import *
import itertools

# import your modules. For example:
from pyschell import Schelling     
from pyphystat.rng import circle_rng
import pyphystat.Pyphystat
from pyphystat.information_index import block_count, information_index

#############################################
# IO and Simulation parameters
#
#############################################
filename = '../data/pd_???????.h5'
groupname = "pd_?????????????"
tablename = 'pd?????'
# set the mode to open the file. See:
# http://pytables.github.io/usersguide/libref/file_class.html#fileclassdescr
iomode= "a"

# constants
nb_time_steps=1

# variables
my_param= [0,1]

# create an iterator to generate all the parameters combinations
l=[my_param]
p = itertools.product(*l)

#############################################
# Initialize hdf5 hierarchy and tables 
# (see pytables tutorial)
#############################################

class Param(IsDescription):
    """the parameters of a simulation """
    my_param       = Int32Col()  
    # some more...........

class Measure(IsDescription):
    """what we measure for a given simulation """
    my_measure = Float32Col()

class PhaseDiagram(IsDescription):
    """after a simulation we record both: parameters and measurements """
    param= Param()
    measure  = Measure()

#############################################
# Initialize hdf5 hierarchy and tables 
# (see pytables tutorial)
#############################################
fileh = openFile( filename, mode = iomode)
root = fileh.root
if not(fileh.__contains__('/'+groupname)):
    group = fileh.createGroup(root, groupname)
table = fileh.createTable("/"+groupname, tablename, 
                          PhaseDiagram, 
                          "phase diagram: "+tablename)
rec = table.row

#############################################
# Phase diagram loop 
# (see pyschelling module)
#############################################

# if you need to create instances of c++ wrapped objects, do it here

for i,param in enumerate(p):
    # unpack parameters
    my_param = param
    # compute initial state
    mat = 0
    # run simulation (in this example, we compute in place)
    run(mat)
    # compute order params
    x= my_measure(mat)
    # save parameters
    rec['param/my_param'] = my_param
    # save measure
    rec['measure/my_measure']= x
    # append
    rec.append()

# flushing is usually done after the loops
table.flush()

fileh.close()
