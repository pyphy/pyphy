CONTENT:

README.txt			: how to run, plot, etc...
gpl-3.0.txt			: license
reardon_schelling_compute.py	: Schelling phase diagram computation
reardon_schelling_plot.py	: plotting
reardon_schelling_notebook.ipynb: an ipython notebook
template_compute.py		: a template file for phase diagram computation
template_plot.py		: a template file for plotting
