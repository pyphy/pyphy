# -*- coding: utf-8
"""
Plot the phase diagram of a Schelling simulation


See:
http://pytables.github.io/usersguide/tutorials.html
http://pytables.github.io/usersguide/tutorials.html#reading-and-selecting-data-in-a-table

"""

import numpy as np
import matplotlib.pylab as plt
from tables import *

#h5file = open_file("tutorial1.h5", mode = "r")       # SYNTAX PYTABLES V3.0
filename = '../data/pd_switch_circle_1.h5'
h5file = openFile(filename, mode = "r")
print h5file

# print raw data
table = h5file.root.pd_switch_circle.pd1
for x in table.iterrows():
    print x, x['param'], x['measure']

# select interface density
# (see 'Reading (and selecting) data in a table' in pytables' tuto)
# pressure = [x['pressure'] for x in table.iterrows() if x['TDCcount'] > 3 and 20 <= x['pressure'] < 50]
f_switch = [ x['param/f_switch'] for x in table.iterrows() if x['param/switch_active']==1]
x_switch = [ x['measure/interface_density'] for x in table.iterrows() if x['param/switch_active']==1]
x_noswitch = [ x['measure/interface_density'] for x in table.iterrows() if x['param/switch_active']==0]

# plot interface density
print "f_switch=",f_switch, "x_switch=",x_switch, "x_noswitch=",x_noswitch
plt.plot(f_switch, x_switch,'b-', label='switch on')
plt.plot(f_switch, x_noswitch,'g--', label='switch off')
plt.legend(); plt.xlabel('f'); plt.ylabel('interface density')
#plt.show()

# plot start/end state
# http://matplotlib.org/examples/images_contours_and_fields/interpolation_methods.html
n= len(f_switch)
for sw in [0,1]:
    state_start = [ x['measure/state_start'] for x in table.iterrows() if x['param/switch_active']==sw]
    state_end = [ x['measure/state_end'] for x in table.iterrows() if x['param/switch_active']==sw]
    l = [ (x['param/f_switch'],x['measure/interface_density']) for x in table.iterrows() if x['param/switch_active']==sw]

    fig, axes = plt.subplots(n,2, figsize=(12, 8),
                             subplot_kw={'xticks': [], 'yticks': []})
    fig.subplots_adjust(hspace=0.3, wspace=0.05)
    fig.suptitle('switch='+str(sw))
    for ax_x, start, end,lab in zip(axes, state_start, state_end,l):
        # ax.imshow(grid, interpolation=interp_method)
        ax_x[0].imshow(start, interpolation='none')
        ax_x[1].imshow(end, interpolation='none')
        bbox = ax_x[0].get_position()
        fig.text(0.03, (bbox.y0+bbox.y1)/2,
                 "f=%1.2f  x=%1.2f"%(lab[0],lab[1]) , ha = 'left')
        

    

plt.show()

h5file.close()
