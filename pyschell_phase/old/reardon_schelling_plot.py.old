# -*- coding: utf-8
"""
Plot the phase diagram of a Schelling simulation
with Reardon information index

See:
http://pytables.github.io/usersguide/tutorials.html
http://pytables.github.io/usersguide/tutorials.html#reading-and-selecting-data-in-a-table

"""

"""
Notes:
* state end: ségreg faible (1 million d iter) => difficile 
* H a un comportement étrange: commence par baisser, puis augmente
  NB: H proche de 1 pour segreg totale, proche de 0 pour 50/50
      explication: comment est considéré le vide ici ?????   

"""

import numpy as np
import matplotlib.pylab as plt
from tables import *

filename = '../data/pd_switch_circle_1.h5' 
iomode = "r"                      # read mode
groupname = "pd_switch_uniform_circle"
tablename = 'pd3'
fs = 20                           # font size

########################################
# Open h5 file
########################################
h5file = openFile( filename, mode = iomode)
group=h5file.root.__getattr__(groupname)
table = group.__getattr__(tablename)
########################################
# plot start/end state
########################################

# What we want to plot:
#            | start| stop activ |stop inactiv|
# unif       |      |            |            |
# circlsmall |      |            |            |
# circllarg  |      |            |            |

f_switch = np.unique([ x['param/f_switch'] for x in table.iterrows()])
n_switch = 3 #f_switch.size

# make axes
fig, axes = plt.subplots(3*n_switch,3)
# titles
axes[0][0].set_title('start', va='top', position=(0.5,1.5))
axes[0][1].set_title('stop inactive', va='top', position=(0.5,1.5))
axes[0][2].set_title('stop active', va='top', position=(0.5,1.5))

# start uniform
req = [ (x['param/f_switch'],x['measure/state_start']) for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==0 and x['param/circle_radius']==40]
req= req[0::4]
i = req.size

for ax_x,r in zip(axes[0:i],req):
    f_switch,state_start = r
    ax_x[0].imshow(state_start, interpolation='none')
    # print f_switch on the left 
    bbox = ax_x[0].get_position()
    fig.text(0.03, (bbox.y0+bbox.y1)/2,
             "f=%1.2f"%(f_switch) , ha = 'left')

# stop uniform inactive
req = [ (x['param/f_switch'],x['measure/state_end']) for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==0 and x['param/circle_radius']==40]
req= req[0::4]
iold =i
i = req.size

for ax_x,r in zip(axes[iold:iold+i],req):
    f_switch,state_end = r
    ax_x[1].imshow(state_end, interpolation='none')

# stop uniform active
req = [ (x['param/f_switch'],x['measure/state_end']) for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==1 and x['param/circle_radius']==40]
req= req[0::4]
iold =i
i = req.size

for ax_x,r in zip(axes[iold:iold+i],req):
    f_switch,state_end = r
    ax_x[2].imshow(state_end, interpolation='none')

# start circle radius 1 
req = [ (x['param/f_switch'],x['measure/state_start']) for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==40]
req= req[0::4]
iold =i
i = req.size

for ax_x,r in zip(axes[iold:iold+i],req):
    f_switch,state_start = r
    ax_x[0].imshow(state_start, interpolation='none')
    bbox = ax_x[0].get_position()
    fig.text(0.03, (bbox.y0+bbox.y1)/2,
             "f=%1.2f"%(f_switch) , ha = 'left')

# stop circle radius 1 switch inactive
req = [ (x['param/f_switch'],x['measure/state_end']) for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==40]

for ax_x,r in zip(axes[3:6],req):
    f_switch,state_end = r
    ax_x[1].imshow(state_end, interpolation='none')

# stop circle radius 1 switch active
req = [ (x['param/f_switch'],x['measure/state_end']) for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==1 and x['param/circle_radius']==40]

for ax_x,r in zip(axes[3:6],req):
    f_switch,state_end = r
    ax_x[2].imshow(state_end, interpolation='none')

# start circle radius 2
req = [ (x['param/f_switch'],x['measure/state_start']) for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==33]

for ax_x,r in zip(axes[6:9],req):
    f_switch,state_start = r
    ax_x[0].imshow(state_start, interpolation='none')
    bbox = ax_x[0].get_position()
    fig.text(0.03, (bbox.y0+bbox.y1)/2,
             "f=%1.2f"%(f_switch) , ha = 'left')

# stop circle radius 2 switch inactive
req = [ (x['param/f_switch'],x['measure/state_end']) for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==33]

for ax_x,r in zip(axes[6:9],req):
    f_switch,state_end = r
    ax_x[1].imshow(state_end, interpolation='none')

# stop circle radius 2 switch active
req = [ (x['param/f_switch'],x['measure/state_end']) for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==1 and x['param/circle_radius']==33]

for ax_x,r in zip(axes[6:9],req):
    f_switch,state_end = r
    ax_x[2].imshow(state_end, interpolation='none')


#plt.show()
########################################
# diag phase
########################################

# What we want to plot:
#           |  interface density |  informaiton index | 
#  uniform  |                    |                    |  
#  circ 1   |                    |                    | 
#  circ 2   |                    |                    |

# make axes
fig, axes = plt.subplots(3,2)

# uniform 
f_switch = [ x['param/f_switch'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==1 and x['param/circle_radius']==33]

x_switch = [ x['measure/interface_density'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==1 and x['param/circle_radius']==33]
x_switchoff = [ x['measure/interface_density'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==0 and x['param/circle_radius']==33]
H_switch = [ x['measure/information_index'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==1 and x['param/circle_radius']==33]
H_switchoff = [ x['measure/information_index'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==0 and x['param/circle_radius']==33]

ax=plt.subplot(3,2,1)
plt.plot(f_switch, x_switch,'b-', label='switch on')
plt.plot(f_switch, x_switchoff,'g--', label='switch off')
plt.legend(loc='upper left'); plt.xlabel('f'); plt.ylabel('interface density')

plt.subplot(3,2,2)
plt.plot(f_switch,np.vstack(H_switch))
plt.plot(f_switch,np.vstack(H_switchoff), '--')
plt.xlabel('f'); plt.ylabel('H')

bbox = ax.get_position()
fig.text(0.03, (bbox.y0+bbox.y1)/2, "uniform" , ha = 'left')

# circle radius 1
rad= 33
x_switch = [ x['measure/interface_density'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==1 and x['param/circle_radius']==rad]
x_switchoff = [ x['measure/interface_density'] for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==rad]
H_switch = [ x['measure/information_index'] for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==1 and x['param/circle_radius']==rad]
H_switchoff = [ x['measure/information_index'] for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==rad]

ax=plt.subplot(3,2,3)
plt.plot(f_switch, x_switch,'b-', label='switch on')
plt.plot(f_switch, x_switchoff,'g--', label='switch off')
plt.legend(loc='upper left'); plt.xlabel('f'); plt.ylabel('interface density')

plt.subplot(3,2,4)
plt.plot(f_switch,np.vstack(H_switch))
plt.plot(f_switch,np.vstack(H_switchoff), '--')
plt.xlabel('f'); plt.ylabel('H')

bbox = ax.get_position()
fig.text(0.03, (bbox.y0+bbox.y1)/2, "circle %d"%rad , ha = 'left')


# circle radius 2
rad=40
x_switch = [ x['measure/interface_density'] for x in table.iterrows() if x['param/init_from_array']==0 and x['param/switch_active']==1 and x['param/circle_radius']==rad]
x_switchoff = [ x['measure/interface_density'] for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==rad]
H_switch = [ x['measure/information_index'] for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==1 and x['param/circle_radius']==rad]
H_switchoff = [ x['measure/information_index'] for x in table.iterrows() if x['param/init_from_array']==1 and x['param/switch_active']==0 and x['param/circle_radius']==rad]

ax=plt.subplot(3,2,5)
plt.plot(f_switch, x_switch,'b-', label='switch on')
plt.plot(f_switch, x_switchoff,'g--', label='switch off')
plt.legend(loc='upper left'); plt.xlabel('f'); plt.ylabel('interface density')

plt.subplot(3,2,6)
plt.plot(f_switch,np.vstack(H_switch))
plt.plot(f_switch, np.vstack(H_switchoff), '--')
plt.xlabel('f'); plt.ylabel('H')

bbox = ax.get_position()
fig.text(0.03, (bbox.y0+bbox.y1)/2, "circle %d"%rad , ha = 'left')


plt.show()

h5file.close()
