# -*- coding: utf-8

"""
Compute the phase diagram of a Schelling simulation 
with variable proportion of switching agents.
Initially, switching agents are located around in circle

Reference:
http://fr.arxiv.org/abs/1212.4929

Python doc:
http://pytables.github.io/usersguide/tutorials.html
(see sections: #dealing-with-nested-structures-in-tables)
also soft/external links

coded by Aurélien Hazan, november 2014
"""

import numpy as np
import matplotlib.pylab as plt
from tables import *

from pyschell import Schelling     
from pyphystat.rng import circle_rng
import pyphystat.Pyphystat

#############################################
# Simulation parameters
#
#############################################
p = {'f_switch' : np.linspace(0,0.3, 5),  'switch_active':[0,1],
     'N': 50, 'tolerance': 0.3, 'Occ':0.7, 'p_u':0.1, 'p_f':0.5, 
     'init': 1, 
     'iteration':0, 'circle_radius': 15, 'circle_sigma': 1.0 }
filename = '../data/pd_switch_circle_1.h5'

#############################################
# Define class structure of data to be recorded 
# in hdf5 format by pytables
# (see pytables tutorial)
#############################################

class Param(IsDescription):
    """the parameters of a simulation """
    iteration       = Int32Col()  
    grid_size       = Int32Col()  
    f_switch        = Float32Col()
    nb_agents_switch= Int32Col()  
    switch_active   = Int32Col()  
    tolerance       = Float32Col()
    occupancy       = Float32Col()
    proba_unhappy   = Float32Col()
    proba_switch    = Float32Col()
    init_from_array = Int32Col()  
    circle_radius   = Float32Col()
    circle_sigma   = Float32Col()

class Measure(IsDescription):
    """what we measure for a given simulation """
    state_start       = Int32Col(shape=(p['N'],p['N']))   
    state_end         = Int32Col(shape=(p['N'],p['N']))  
    interface_density = Float32Col()
    
class PhaseDiagram(IsDescription):
    """after a simulation we record both: parameters and measurements """
    param    = Param()
    measure  = Measure()

#############################################
# Initialize hdf5 hierarchy and tables 
# (see pytables tutorial)
#############################################
#fileh = open_file( filename, mode = "w")   #  SYNTAX: pytables V3.0
fileh = openFile( filename, mode = "w") 
root = fileh.root
groupname = "pd_switch_circle"
#group = fileh.create_group(root, groupname)  #  SYNTAX: pytables V3.0
group = fileh.createGroup(root, groupname)  
grec = root.pd_switch_circle
tablename = 'pd1'
#table = fileh.create_table("/pd_switch_circle", tablename, Record,  #SYNTAX
table = fileh.createTable("/"+groupname, tablename, PhaseDiagram, 
                           "phase diagram: "+tablename)
rec = table.row

#############################################
# Phase diagram loop 
# (see pyschelling module)
#############################################

# Create objects from external modules
sim = Schelling.__dict__["Sim"]
iface_dens =  pyphystat.Pyphystat.__dict__["interface_density"]
i=0; imax = p['f_switch'].size*len(p['switch_active'])

for sw in p['switch_active']:
    for f in p['f_switch']:
        # compute param
        nb_agents_switch = np.int((p['N']**2)*f*p['Occ']+1)
        # compute initial state
        mat = circle_rng(p['N'], nb_agents_switch, 
                         p['circle_radius'], p['circle_sigma'],[])
        mat0 = np.copy(mat)
        # run simulation
        sim(mat, p['tolerance'], p['Occ'],nb_agents_switch, 
            p['p_u'], p['p_f'], p['init'], sw)
        # compute order params
        x=iface_dens(mat)
        # save parameters
        rec['param/circle_radius'] = p['circle_radius']
        rec['param/circle_sigma'] = p['circle_sigma']
        rec['param/grid_size'] = p['N']
        rec['param/iteration'] = p['iteration']
        rec['param/f_switch'] = f
        rec['param/nb_agents_switch'] = nb_agents_switch
        rec['param/switch_active'] = sw
        rec['param/tolerance'] = p['tolerance']
        rec['param/occupancy'] = p['Occ']
        rec['param/proba_unhappy'] = p['p_u']
        rec['param/proba_switch'] = p['p_f']
        rec['param/init_from_array'] = p['init']
        rec['param/f_switch'] = f
        # save measure
        rec['measure/state_start'] = mat0
        rec['measure/state_end'] = mat
        rec['measure/interface_density'] = x
        rec.append()
        # print %done
        print "%2.f "%(float(i)/float(imax)*100.), sw, f
        i=i+1

# flushing is usually done after the loops
table.flush()

fileh.close()

