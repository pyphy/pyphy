# -*- coding: utf-8

"""
Compute the phase diagram of a Schelling simulation 
with various geometries of switching agents.

Writes the results in a hdf5 file, with pytables.

To plot the results: see thisfilename_plot.py

Reference:
http://fr.arxiv.org/abs/1212.4929

Python doc:
pytables:   http://pytables.github.io/usersguide/tutorials.html
            (see sections: #dealing-with-nested-structures-in-tables)
            see also soft/external links
itertools:  https://docs.python.org/2/library/itertools.html
matplotlib: http://matplotlib.sourceforge.net

coded by Aurélien Hazan, november 2014
License: see README.txt
"""

"""
PROBLEMS:
* save space:  (can be a problem is we record large monte-carlo simulations)
  do not record constants in Measure (or use a link).
  example: histogram_bins
* members of Param Measure must be saved individually, which is time consuming.
  Can it be done automatically ? Example:
    rec['param/grid_size'] = N
    rec['param/nb_time_steps']=nb_time_steps
* Can Param and Measure classes be built automatically ?
* gridsize N must be constant, for state matrix to be recorded in pytables
 (because in the definition of class "Measure", matrix size must be set. 
  Example:  Int32Col(shape=(N,N)) )
 => howto run experiments with different N ??? 
**define the classes as objects of another class inside another ?
** subclass IsDescription and redefine the constructor ?
* tablename: set it automatically
"""

# import general purpose modules
import numpy as np
import matplotlib.pylab as plt
from tables import *
import itertools

# import simulation and data-processing modules
from pyschell import Schelling     
from pyphystat.rng import circle_rng
import pyphystat.Pyphystat
from pyphystat.information_index import block_count, information_index


#############################################
# IO and Simulation parameters
#
#############################################
filename = '../data/pd_switch_circle_1.h5'
groupname = "pd_switch_uniform_circle"
tablename = 'pd3'
# set the mode to open the file. See:
# http://pytables.github.io/usersguide/libref/file_class.html#fileclassdescr
iomode= "a" 

# constants
nb_time_steps=5000000
iteration=0
N =100
tolerance= 0.3
occupancy=0.7
p_u=0.1
p_s=0.01
circle_sigma=1.0
blocksize=np.array([5,10,20])
n_blocksize = blocksize.size

# variables
switch_active= [0,1]
uniform_or_circle=[0,1]
circle_radius=[N/2-10, N/3]
f_switch=np.linspace(0,0.4, 10)

# create an iterator to generate all the parameters combinations
l=[switch_active,uniform_or_circle,circle_radius,f_switch]
p = itertools.product(*l)

#############################################
# Initialize hdf5 hierarchy and tables 
# (see pytables tutorial)
#############################################
class Param(IsDescription):
    """the parameters of a simulation """
    iteration       = Int32Col()  
    nb_time_steps   = Int32Col()  
    grid_size       = Int32Col()  
    f_switch        = Float32Col()
    nb_agents_switch= Int32Col()  
    switch_active   = Int32Col()  
    tolerance       = Float32Col()
    occupancy       = Float32Col()
    proba_unhappy   = Float32Col()
    proba_switch    = Float32Col()
    init_from_array = Int32Col()  
    circle_radius   = Float32Col()
    circle_sigma    = Float32Col()
    blocksize       = Int32Col(shape=blocksize.shape)  

class Measure(IsDescription):
    """what we measure for a given simulation """
    time_converge     = Int32Col()  
    state_start       = Int32Col(shape=(N,N))    
    state_end         = Int32Col(shape=(N,N))  
    interface_density = Float32Col()
    information_index = Float32Col(shape=blocksize.shape) 

    
class PhaseDiagram(IsDescription):
    """after a simulation we record both: parameters and measurements """
    param= Param()
    measure  = Measure()



#############################################
# Initialize hdf5 hierarchy and tables 
# (see pytables tutorial)
#############################################
fileh = openFile( filename, mode = iomode)
root = fileh.root
if not(fileh.__contains__('/'+groupname)):
    group = fileh.createGroup(root, groupname)
table = fileh.createTable("/"+groupname, tablename, 
                          PhaseDiagram, 
                          "phase diagram: "+tablename)
rec = table.row

#############################################
# Phase diagram loop 
# (see pyschelling module)
#############################################

# create instances of c++ wrapped objects
sim = Schelling.__dict__["Sim"]
iface_dens =  pyphystat.Pyphystat.__dict__["interface_density"]
imax = 1
mat=[]

for i,param in enumerate(p):
    # unpack parameters
    switch_active,uniform_or_circle,circle_radius,f_switch = param
    
    # compute initial state
    mat = np.zeros((N,N), 'i')
    nb_agents_switch = np.int((N**2)*f_switch*occupancy+1)
    if(uniform_or_circle):        
        mat = circle_rng(N, nb_agents_switch, 
                         circle_radius, circle_sigma,[])
    
    mat0 = np.copy(mat)    
    # run simulation
    try:
        time_converge=sim(mat, tolerance, occupancy, f_switch, 
                          p_u, p_s, uniform_or_circle, switch_active, 
                          nb_time_steps)
    except:
        print 'simulation failed'
        time_converge=-1
    # compute order params
    x=iface_dens(mat)
    H = np.zeros(blocksize.size)
    for j,bs in enumerate(blocksize):
        count = block_count(mat,bs)
        H[j] = information_index(count)
    # save parameters
    rec['param/circle_radius'] = circle_radius
    rec['param/circle_sigma'] = circle_sigma
    rec['param/grid_size'] = N
    rec['param/nb_time_steps']=nb_time_steps
    rec['param/iteration'] = iteration
    rec['param/f_switch'] = f_switch
    rec['param/nb_agents_switch'] = nb_agents_switch
    rec['param/switch_active'] = switch_active
    rec['param/tolerance'] = tolerance
    rec['param/occupancy'] = occupancy
    rec['param/proba_unhappy'] = p_u
    rec['param/proba_switch'] = p_s
    rec['param/init_from_array'] = uniform_or_circle
    rec['param/blocksize'] = blocksize
    # save measure
    rec['measure/time_converge']=time_converge
    rec['measure/state_start'] = mat0
    rec['measure/state_end'] = mat
    rec['measure/interface_density'] = x
    rec['measure/information_index'] = H
    # append
    rec.append()
    # print %done    
    print "%2.f /100"%(100.*float(i)/float(imax) )


# flushing is usually done after the loops
table.flush()

fileh.close()
