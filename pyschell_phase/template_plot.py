# -*- coding: utf-8
"""
Plot the phase diagram of ...

See:
http://pytables.github.io/usersguide/tutorials.html
http://pytables.github.io/usersguide/tutorials.html#reading-and-selecting-data-in-a-table

"""

# remark that we only import general purposes modules
# (no simulation modules needed here)

import numpy as np
import matplotlib.pylab as plt
from tables import *

########################################
# Parameters
########################################

filename = '../data/pd_???????.h5' 
iomode = "r"                              # read mode
groupname = "pd_????"
tablename = 'pd???'
fs = 20

########################################
# Open h5 file
########################################
h5file = openFile( filename, mode = iomode)
group=h5file.root.__getattr__(groupname)
table = group.__getattr__(tablename)

########################################
# Get and plot data
########################################

# make a request
req_all = [ (x['param/my_param'],x['measure/my_measure']) for x in table.iterrows()]
# you may want a subset only
req_subset = [ (x['param/my_param'],x['measure/my_measure']) for x in table.iterrows() if x['param/my_param']==0 and 1]

# plot
nb_req = len(all)
fig, axes = plt.subplots(nb_req,1)

for ax_x,r in zip(axes,req):
    # unpack request
    param, measure = req
    # plot. For example an image:
    ax_x[0].imshow(measure, interpolation='none')
    # get the bounding box of the image
    bbox = ax_x[0].get_position()
    # print the parameter value alongside
    fig.text(0.03, (bbox.y0+bbox.y1)/2,
             "p=%1.2f"%(param) , ha = 'left')

# show us the plot
plt.show()
# close the file
h5file.close()
