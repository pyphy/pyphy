 # -*- coding: utf-8

"""
random number generator
"""
import matplotlib.pyplot as plt
import numpy as np
from numpy.random import seed,normal, uniform, randint

def circle_rng(gridsize, n_pts, rad, sig, rnd_seed):
    """
    add a random circle populated with {-1,+1} to a zero (integer) matrix
    """
    if (not(rnd_seed==[])):
        seed(rnd_seed)

    grid = np.zeros((gridsize,gridsize),'i')
    i_pts=0
    i = 0
    imax = 100*n_pts
    while ((i_pts<n_pts) & (i<imax)):
        # sample radius from gaussian
        r = normal(rad,sig)
        # sample angle from uniform
        theta = uniform(0, 2*np.pi)
        # convert position
        x= r* np.cos(theta)
        y= r* np.sin(theta)
        # check point in box
        xlim = float(gridsize)/2.
        if((x<xlim) & (x>= -xlim) & (y<xlim) & (y>=-xlim)):
            x_int = int(x+xlim)
            y_int = int(y+xlim)
            #print x_int,y_int
            if(grid[x_int,y_int]==0):
                grid[x_int,y_int]= -1+2*randint(0,2)    #  =1
                i_pts +=1
        i+=1

    return grid

if __name__ == "__main__":
    gridsize=100
    n_pts=100
    rad=10
    sig=1
    rnd_seed=1
    g = circle_rng(gridsize, n_pts, rad, sig, rnd_seed)
    plt.matshow(g)
    plt.show()
