# -*- coding: utf-8

"""
Get block histogram from Schelling simulation matrix
as in Durrett and Zhang (see [DZ14]) 

Reference:
[DZ14] Durrett, Zhang 10.073/pnas.1414915111
[DZ14SI] supporting information text.

coded by Aurélien Hazan
"""

"""
TODO:
* recode count_goodval
"""

import itertools
import numpy as np
from functools import partial
import matplotlib.pyplot as plt

def count_goodval(x, goodval):
    """
    For each element of goodval, count the number of occurrences
    in array x.

    see: http://docs.scipy.org/doc/numpy/reference/generated/numpy.where.html#numpy-where
    NOT EFFICIENT!! => RECODE WITH NUMPY FUNCTION
    
    In:
    ---
    x : array, shape=(nr,nc)
    
    goodval: list, len=n
    
    Out:
    ---

    l: list, len=n
    count, for each element of goodval

    """
    l=[]
    for g in goodval: 
        l.append(len(np.where(x==g)[0]))
    return l

def apply_submat(mat, blocksize, f):
    """
    compute a function on submatrices

    In:
    ---
    mat: array, shape=(nr,nc)
    
    blocksize: integer
    size of the submatrices

    f: function
    function to be applied to each submatrix
    
    Out:
    ---
    l: list
    values of f(submat), for each submatix of mat
    
    """
    nr,nc=mat.shape[0],mat.shape[1]
    lr = range(0,nr,blocksize)
    lc = range(0,nc,blocksize)
    idx_r = zip(lr[0:-1],lr[1:])
    idx_c = zip(lc[0:-1],lc[1:])
    
    l=[idx_r,idx_c]
    p = itertools.product(*l)
    res=[]
    for param in p:
        ir,ic = param
        submat =mat[slice(ir[0],ir[1],None), slice(ic[0],ic[1],None)]
        res.append(f(submat))
    return res

def get_initial_state(N,L,dens):
	"""
	Compute block histogram from random array, with square block.
		
	Parameters:
	-----------
	N: int
	the total number of cells is N**2
	
	L: int
	number of neighbors in neighborhood. 
	
	density: list
	[densA,densB] are the normalized densities of A/B 	
	
	Returns:
	--------
	H: array, shape=[sqrt(L)+1, sqrt(L)+1]	
	2d histogram
	"""	
	if(not isinstance(dens,list)):	
		raise ValueError()
	[densA,densB]=dens	
	if(densA+densB>1):
		raise ValueError()
	# get a random matrix {-1,+1,0}	
	blocksize =int(np.sqrt(L))
	mat = np.zeros(N**2)
	nA = int(np.floor(densA*float(N**2)))
	nB = int(np.floor(densB*float(N**2)))
	mat[0:nA]=1
	mat[nA:nA+nB]=-1
	idx = np.arange(N**2)
	idx = np.random.permutation(idx)
	mat=mat[idx]
	mat=mat.reshape(N,N)
	# compute block content
	f= partial(count_goodval, goodval=[-1,1])
	l=apply_submat(mat, blocksize,f)
	m = np.array(l)		
	bins = np.arange(blocksize**2+2)
	# compute histogram of block content
    # http://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram2d.html#numpy.histogram2d
	H,xedges,yedges=np.histogram2d(m[:,0],m[:,1], bins=bins)
	return H,xedges


def get_initial_state_deprecated(N,L,density):
	"""
	Compute block histogram from random array, with square block.
		
	Parameters:
	-----------
	N: int
	the total number of cells is N**2
	
	L: int
	number of neighbors in neighborhood. 
	
	density: float
	1 if all sites are filled with -1/+1, 0 if all set to 0
	
	Returns:
	--------
	H: array, shape=[sqrt(L)+1, sqrt(L)+1]	
	2d histogram
	"""	
	blocksize =int(np.sqrt(L))
    # get a random matrix {-1,+1}
	mat = np.random.randint(0,2,N*N).reshape(N,N)
	mat[mat==0]=-1
	# set a given fraction to 0
	idx = np.arange(N**2)
	idx = np.random.permutation(idx)
	idx = idx[0: np.floor((1.0-density)* np.float(N**2)) ]
	mat.flat[idx]=0
	# compute block content
	f= partial(count_goodval, goodval=[-1,1])
	l=apply_submat(mat, blocksize,f)
	m = np.array(l)		
	bins = np.arange(blocksize**2+2)
	# compute histogram of block content
    # http://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram2d.html#numpy.histogram2d
	H,xedges,yedges=np.histogram2d(m[:,0],m[:,1], bins=bins)
	return H,xedges

def count_agent_histogram(H):
	"""
	compute the number of A/B agents 
	number of A =sum h_ij *i, number of B =sum h_ij *j
	
	Parameters:
	-----------
	H: array, shap=(n,n)
	H[i,j]: number of blocks containing i agents of type A,
	and	j agents of type A
	
	Returns:
	--------
	nb_A: integer
	total number of type A agents 
	
	nb_B: integer
	total number of type B agents 
	
	See also:
	---------
	pyphystat.block.get_initial_state()
	
	"""
	nr,nc = H.shape
	nb_agent = np.arange(nr)
	nb_A  = np.dot( H.sum(0) , nb_agent)
	nb_B  = np.dot( H.sum(1) , nb_agent)
	return nb_A,nb_B




def test():
	"""
	In:
	---
	Out:
	---
	
	"""
	#typeCode = "i"
	N = 100  
	blocksize =3
	density=[0.5,0.2]
	H,xedges = get_initial_state(N,blocksize**2, density)
	print H
	print xedges	
    # plot
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.set_title('pcolormesh: exact bin edges')
	X, Y = np.meshgrid(xedges, xedges)
	ax.pcolormesh(X, Y, H)
	ax.set_aspect('equal')
	plt.show()
    	
			
if __name__ == "__main__": 
    test()
