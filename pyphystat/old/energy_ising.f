C========+=========+=========+=========+=========+=========+=========+=$
C PROGRAM : energy-ising (alg. 5.1)
C TYPE    : real function
C PURPOSE : computing the energy of an Ising-model configuration.
C           Nbr(.,.) encodes the neighbor scheme 
C VERSION : 03-JAN-2005
C COMMENT : Here we sum over all neighbors (but divide by 2). In the 
C           printed routine on page 231, the sum goes over half the neighbors.
C AUTHOR  : W. Krauth
C LANGUAGE: Fortran 77 (mod)
C========+=========+=========+=========+=========+=========+=========+=$
      function energy(Is,Nsites,Nbr,ncoord)
      dimension Is(Nsites),Nbr(ncoord,Nsites)
      energy=0
      do i=1,Nsites
         do j=1,ncoord
            if (nbr(j,i).ne.0) energy=energy + Is(i)*Is(nbr(j,i))
         end do
      end do
      energy=-energy/2
      end
