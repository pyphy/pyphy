/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#include <iostream>
#include <cstdlib>

#include "energy_blume_capel.h"
#include "spin_state_2D.h"


void print_mat(int**mat , int nx, int ny)
{
  for(int i=0; i<nx; i++)
    {
      for(int j=0; j<ny; j++)
	std::cout << mat[i][j] <<"\t";
      std::cout << std::endl;  
    }
      
}

void fill_test_mat(int **mat, int nx, int ny,  int label_plus_one, int label_minus_one)
{
  // random number generation
  double u;
  
  // initialize
  for(int i=0; i<nx; i++)
    for(int j=0; j<ny; j++)
      {
	u=rand();
	div_t  temp;
	temp = div(u,3);
	
	if(temp.rem==0) 
	  mat[i][j]=0;
	else{
	  if(temp.rem==1) 
	    mat[i][j]=label_plus_one;
	  else
	    mat[i][j]=label_minus_one;
	}
      }
}


int** make_test_mat(int nx, int ny, int label_plus_one, int label_minus_one)
{
 
  // allocation
  int **mat =  new int*[nx];
  for(int i=0; i<nx; i++)
    mat[i] = new int[ny];
  
  // init
  fill_test_mat(mat,nx,ny,label_plus_one,label_minus_one);
  
  return mat;
}

void delete_test_mat(int **mat, int nx){
  for(int i=0; i<nx; i++)
    delete mat[i] ;
  delete mat;
    
}


int main( void)
{
  // random number generation
  srand( time(NULL) );

  /* create matrix with elements taken in 
     {0, label_plus_one, label_minux_one}
  */
  
  int nx = 5;
  int ny= 10;
  int label_plus_one=1 ;
  int label_minus_one=2;
  int** mat = make_test_mat(nx,ny, label_plus_one, label_minus_one); 
 
  /* create state and neighbour */
  int ndim = 2;
  int nneighbour=4;
  neighbour_2D n(nneighbour,nx,ny);
  state_2D s(mat, nx, ny, label_plus_one, label_minus_one);
  int nsites = nx*ny;
  double K = 1.0;

  // compute energy
  state* s_ptr = static_cast<state*>( &s);
  neighbour* n_ptr = static_cast<neighbour*>(&n);
  int ei = energy_ising( s_ptr, n_ptr, nsites, ndim );
  double ebc= energy_blume_capel( s_ptr, n_ptr, nsites, ndim, K);

  // print
  print_mat(mat,nx,ny);
  std::cout<<"energy ising="<< ei << " energy blume-capel="<< ebc << std::endl;

  // updata data and recompute
  fill_test_mat(mat,nx,ny, label_plus_one, label_minus_one);
  s.update_data(mat);
  ebc= energy_blume_capel( s_ptr, n_ptr, nsites, ndim, K);

  // print
  print_mat(mat,nx,ny);
  std::cout<<"energy ising="<< ei << " energy blume-capel="<< ebc << std::endl;
  
  //free memory
  delete_test_mat(mat,nx);
  
  return 0;
}
