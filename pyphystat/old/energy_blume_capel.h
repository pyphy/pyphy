/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#ifndef _ENER_BLUME_CAPEL_
#define _ENER_BLUME_CAPEL_

#include "spin_state.h"

double energy_ising(state *s, neighbour *n, int nsites, int ndim );
double energy_blume_capel(state *s, neighbour *n, int nsites, int ndim, double K);
  


#endif
