/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

/**********************************************************************
 * PROGRAM: energy_blume_capel
 * PURPOSE: computing the energy of an Ising-model configuration.
 *
 * VERSION : 28 june 2012
 * COMMENT : inspired by W.Krauth's book, algorithm 5.1
 *           see http://www.smac.lps.ens.fr/index.php/Main_Page
 * AUTHOR  : Aurelien Hazan
 * LANGUAGE: C++
 *
 **********************************************************************/
#include "energy_blume_capel.h"
#include <iostream>

#define DEBUG 0

double energy_ising(state* s, neighbour* n, int nsites, int ndim ){
  int energy = 0;
  int spin, spin_neigh;
  int nji;
  
  if(DEBUG)
    std::cout <<"energy_ising: nsites="<<nsites <<" ndim="<<ndim<<std::endl;
  
  for(int i=0; i<nsites; i++)
    for(int j=0; j<ndim; j++)
      {
	nji= n->get(j,i) ;
		
	if (nji != 0)
	  {	
	    spin = s->get(i);
	    spin_neigh = s->get(nji);
	    energy = energy + spin * spin_neigh ;
	      }

	if(DEBUG)
	  std::cout <<"i="<<i <<" j="<<j <<" nji=" <<nji<<std::endl;
      }
  
  return -1.0*((double)energy)/2.0;
}


double energy_blume_capel(state* s, neighbour* n, int nsites, int ndim, double K ){
  
  int spin, spin_neigh;
  double energy = 0.0;
  int nji;
  
  if(DEBUG) std::cout << "EBC: state=" <<s<< " neigh="<<n<<" nsites="<<nsites<<" ndim="<<ndim<<" K="<<K<<std::endl;
  
  for(int i=0; i<nsites; i++)
    for(int j=0; j<ndim; j++)
      {
	if(DEBUG) std::cout<<"i="<<i<<" j="<<j<<std::endl;
	nji= n->get(j,i) ;
	if(DEBUG) std::cout<<"EBC: nji="<<nji<<std::endl;

	if (nji != 0)
	  {
	    spin = s->get(i);
	    spin_neigh = s->get(nji);
	    energy = energy + spin * spin_neigh 
	      +  K *( spin*spin * spin_neigh*spin_neigh);
	    
	    if(DEBUG) std::cout<<"EBC: spin="<<spin<<" spin_neigh="<<spin_neigh<<" energy="<<energy<<std::endl;
			    
	  }
      }
  return -energy/2.0;
}
