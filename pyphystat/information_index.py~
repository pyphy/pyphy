 # -*- coding: utf-8

"""
Implementation of information index computation.
First, group data in blocks with block_count()
Then compute information_index()

coded by Aurélien Hazan.
"""
import matplotlib.pyplot as plt
import numpy as np

def block_count(mat, block_size):
    """
    divide a integer matrix in blocks and count the number of elements 
    in each block
    """
    row,col = mat.shape
    I,J=np.floor_divide(row, block_size), np.floor_divide(col, block_size)
    labels = np.unique(mat)
    count = np.zeros((I*J,labels.size)) 
    for i in range(row):
        d_i = np.floor_divide(i, block_size)
        for j in range(col):
            d_j = np.floor_divide(j, block_size)
            x = mat[i,j]
            idx=np.where(x==labels)[0][0]
            count[I*d_i+d_j,idx] += 1

    return count


def information_index(dat):
    """
    information index, adapted after 'spatseg' by Seong-Yun Hong
    """
    dat = dat.astype('float')
    # replace zeros
    tol = 1e-10
    idx0 = (dat==0)
    dat[idx0]=tol
    
    m = dat.shape[1]
    # Total population in the study area
    ptsSum = np.sum(dat)
    # Population of all groups at each data point
    ptsRowSum = np.sum(dat,1)
    # Total population of each subgroup
    ptsColSum = np.sum(dat,0)
    # Proportion of each subgroup
    ptsProp = ptsColSum.astype('float') / ptsSum.astype('float')
    # Population proportion of each group at each local environment
    envProp = np.transpose(np.outer(np.ones((1,m)), np.sum(dat,1)))
    envProp = dat / envProp   

    def logistic(x):
        return np.sum( x * np.log(x)/np.log(m) )

    Ep = -1 * np.apply_along_axis(logistic, 1 ,envProp) 
    E = -logistic(ptsProp)
    H = 1. - (np.sum(ptsRowSum * Ep) / (ptsSum * E))
    #Ep <- apply(envProp, 1, function(z) sum(z * log(z, base = m))) * -1
    #E <- sum(ptsProp * log(ptsProp, base = m)) * -1
    #H <- 1 - (sum(ptsRowSum * Ep) / (ptsSum * E))
    return H


if __name__ == "__main__":
    n=5
    mat = np.ones((n,n))
    h1 = np.hstack( (np.ones((n,n)), -1* np.ones((n,n))) )
    h2 = np.hstack( (-1* np.ones((n,n)), np.ones((n,n))) )
    M = np.vstack( (h1, h2)) 

    count = block_count(M, n)
    print count
    print information_index(count)

    count=np.array([[50,50],[50,50],[50,50],[50,50]])
    print "===========\n"
    print "count=",count
    print "information_index",information_index(count)
    
    count=np.array([[33,33,33],[33,33,33]])
    print "===========\n"
    print "count=",count
    print "information_index",information_index(count)

	
    count=np.array([[1,0,0],[0,1,0]])
    print "===========\n"
    print "count=",count
    print "information_index",information_index(count)

