# -*- coding: utf-8

"""
test python wrapper to c++ functions in Pyphystat.cxx
(e.g. interfac edensity function iface_dens())

coded by Aurélien Hazan
"""

# Import NumPy
import numpy as np
from numpy.random import randint
import Pyphystat

# instantiate the c++ function object
iface_dens = Pyphystat.__dict__["interface_density"]

# fully segregated
zero =  np.zeros((50,50),'i')
zero[:,25:50]=1
x= iface_dens(zero+1) # +1: because interface with 0 are discarded
print "fully segregated matrix, x=", x

# random matrix
n = 30
mat = randint(0,2,n*n).reshape((n,n))
x= iface_dens(mat+1) # +1: because interface with 0 are discarded
print "random matrix, x=", x

# regular alternance
a = np.array([0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1])
x=iface_dens(a.reshape((4,5))+1) # +1: because interface with 0 are discarded
print "regular alternance=",x
